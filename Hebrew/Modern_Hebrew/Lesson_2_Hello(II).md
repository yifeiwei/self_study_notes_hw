#### [Lesson 2 Hello (II)](https://www.bilibili.com/video/av10177199)

##### 1.5 Where are you from?

| English                                    | Hebrew                  |
| ------------------------------------------ | :---------------------- |
| Taxi driver: Are you from America?         | אתם מאמריקה             |
| Mr Green: No, not from America.            | לא, לא מאמריקה          |
| Taxi driver: From Canada?                  | **מ**קנדה               |
| Mrs Green: Not from Canada. From England.  | לא מקנדה. מאנגליה       |
| Taxi driver: Are you tourists in Israel?   | אתם תיירים בישראל       |
| Mrs Green: Yes, we are tourists in Israel. | כן, אנחנו תיירים בישראל |

* 如果מ后面是元音类似א，发音要发成me，否则的话发成mi

אמריקה: America

קנדה: Canada

אנגליה: England

תייר: tayar= tourist

תיירים: tourists

ישראל: Israel

ב: 在

נהג: driver nehag

 טקסי: taxi

人称代词复习

* אני: 
* אתה, את אתם, אתן

##### Vocabulary

| Hebrew        | Transliteration | English     |
| ------------- | --------------- | ----------- |
| לגור ה....  כ | lagur be        | live in     |
| ב             | be              | in          |
| מ             | me              | from        |
| איפה          | eifo            | where       |
| כן            | ken             | so          |
| לא            | lo              | no          |
| ישראלי        | israeli         | Israeli     |
| ישראל         | israel          | Israel      |
| אמריקני       | americani       | American(m) |
| אמריקה        | america         | America     |
| אנגלי         | angli           | English(m)  |
| אנגליה        | angliah         | England     |
| קנדי          | kanadi          | Canadian(m) |
| קנדה          | kanada          | Canada      |
| תירים         | tayarim         | tourists    |
| נהג           | nehag           | driver(m)   |

##### Gramma: present tense

| Verb        | Personal pronoun singular | Verb         | Personal pronoun plural |
| ----------- | ------------------------- | ------------ | ----------------------- |
| גר\|גרה     | אני                       | גרים\|גרוד   | אנחנו                   |
| I live      | I                         | We live      | We                      |
| גר          | אתה                       | גרים         | אתם                     |
| gar         | atah                      | garim        | atem                    |
| you(m) live |                           | you(m) live  |                         |
| גרה         | את                        | גרות         | אתן                     |
| garah       | at                        | garot        | aten                    |
| you(f) live |                           | you(f) live  |                         |
| גר          | **הוא**                   | גרים         | **הם**                  |
| gar         | hoo                       | garim        | hem                     |
| he lives    |                           | they(m) live |                         |
| גרה         | **היא**                   | גרום         | **הן**                  |
| garat       | hee                       | garot        | hen                     |
| she lives   |                           | they(f) live |                         |

Therefore,

| masculine singular | feminine singular | masculine plural | feminine plural |
| ------------------ | ----------------- | ---------------- | --------------- |
| גר                 | גרה               | גרים             | גרות            |

| English                                                 | Hebrew                                          |
| ------------------------------------------------------- | ----------------------------------------------- |
| Shalom: David, do you speak Hebrew?                     | שלום: דויד, אתהמדבר עבר                         |
| David: No, I don't speak Hebrew. I speak only English.  | דויד: לא, אני לא מדבר עברית. אני מדבר רק אנגלית |
| Ruth: I speak Hebrew and English. Do you speak English? | רות: אני מדברת עברית ואנגלית. אתה מדבר אנגלית   |
| Shalom: I speak English and also Hebrew.                | שלום: אני מדבר אנגלית וגם עברית                 |

| Hebrew | Transliteration | English   |
| ------ | --------------- | --------- |
| לדבר   | ledaber         | to speak  |
| עברית  | ivrit           | Hebrew    |
| רק     | rak             | only      |
| אנגלית | angrit          | English   |
| גם     | gam             | also, too |

| masculine singular I, you he | feminine singular I, you, she | masculine plural we, you, they | feminine plural we, you, they |
| ---------------------------- | ----------------------------- | ------------------------------ | ----------------------------- |
| מדבר                         | מדברת                         | מדברים                         | מדברות                        |

| Country | Language | Nationality |
| ------- | -------- | ----------- |
| ישראל   | עברית    | ישראלי      |
| רומניעה | רומנית   | רומני       |
| קנדה    | אנגרית   | קנדי        |
| הולנד   | הולנדית  | הולנדי      |
| רוסיה   | רוסית    | רוסי        |
| ירדן    |          | ירדני       |

