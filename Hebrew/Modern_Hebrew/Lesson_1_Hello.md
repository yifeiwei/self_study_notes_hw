### Modern Hebrew from Scratch
![img](http://www.behrmanhouse.com/sites/default/files/handwritingCropped.jpg) 
#### [Lesson 1 Hello](https://www.bilibili.com/video/av10160665)

#### [Lesson 1 Hello](https://www.bilibili.com/video/av10160665)

##### 1.1 Greeting

| English      | Transliteration |  Hebrew  |
| ------------ | :-------------- | :------: |
| Hello        | shalom          |   שלום   |
| Good morning | boker tov       | בוקר טוב |
| Good evening | erev tov        | ערב טוב  |
| Good night   | layla tov       | לילה טוב |

##### 1.2 Who are you?

| English                               | Transliteration                  | Hebrew                      |
| ------------------------------------- | -------------------------------- | --------------------------- |
| Sara: Hello, who are you?             | Sara: shalom mi atah?            | שרה: שלום, מי אתה           |
| Dan: I am Dan, who are you?           | Dan: ani Dan, mi at?             | דן: אני דןת מי את           |
| Mr Golan: I am Mr Golan, who are you? | mar Golan: ani mar Golan, mi at? | מר גולן: אני מר גולןת מי את |
| Ms Tavor: I am Ms Tavor.              | marat Tavor: ani marat Tavor.    | מרת: אני מרת תבור           |

מי: Who

מר = Mr.

מרת = Ms.

אתה: 你

את: 妳

##### 1.3 Mr and Mrs Green, Mr Cohen

| English                                               | Transliteration                            | Hebrew                            |
| ----------------------------------------------------- | ------------------------------------------ | --------------------------------- |
| Mr Cohen: Hello, I am Mr Cohen, who are you? (plural) | mar Kohen: shalom, ani mar Kohen, mi atem? | מר כוהן: שלום, אני מרכוהן, מי אתם |
| Mr Green: Hello, I am Mr Green.                       | mar Green: shalom, ani mar Green.          | מר גרין: שלום, אני מר גרין        |
| Mrs Green: I am Mrs Green.                            | marat Green: ani marat Green.              | מרת כרין: אני מרת גרין            |
| Mr Cohen: Pleased to meet you!                        | mar Kohen: naim meod                       | מר כוהן: נעים מאוד                |

אתם: 你们

נעים מאוד: very happy (happy very)

* Self-introduction

| English                                         | Transliteration                                 | Hebrew                         |
| ----------------------------------------------- | ----------------------------------------------- | ------------------------------ |
| Shalom Cohen: Hello, I am Shalom, Shalom Cohen. | Shalom Kohen: shalom, ani Shalom, Shalom Kohen. | שלום כרהן: שלום, אני שלום כוהן |
| Ruth Green: I am Ruth, Ruth Green.              | Rut Green: ani Rut, Rut Green.                  | רות גרין: אני רות, רות גרין    |
| David Green: I am David, David Green.           | David Green: ani David, David Green             | דויד גרין: אני דויד, דויד גרין |

* How are you?

| English                               | Transliteration                        | Hebrew                           |
| ------------------------------------- | -------------------------------------- | -------------------------------- |
| Shalom Cohen: Hello, how are you?     | Shalom Cohen: Shalom, mah shlomchem?   | שלום כוהן: שלום, מה שלומכם       |
| Ruth Green: I'm well, thank you.      | Ruth Green: todah, tov.                | רות גרין: תודה, טוב              |
| David Green: I am very well. And you? | David Green: shlomi tov meod. ve atah? | דויד גרין: שלומי טוב מאוד. ו אתה |
| Shalom Cohen: Fine, thank you.        | Shalom Cohen: tov, todah.              | שלום כוהן: טוב תודה              |

שלומ**כם**  : 你们的平安

מה: what

שלומ**י**: 我的平安

| English            | Transliteration | Hebrew    |
| ------------------ | --------------- | --------- |
| How are you? (m/s) | mah shlomcha?   | מה שלומך  |
| How are you? (f/s) | mah shlomech?   | מה שלומך  |
| How are you? (m/p) | mah shlomchem?  | מה שלומכמ |
| How are you? (f/p) | mah shlomchen?  | מה שלוכן  |

