### Convolutional Neural Network

#### 1.1 Computer Vision

Computer vision problems

* Image Classification
* Object detection
* Neural style transfer

Deep Learning on Large images

#### 1.2 Edge Detection example 

Vertical edge detection

filter (kernel), convolution

#### 1.3 More edge detection

Vertical and horizontal Edge detection

Learning to detect edges

```math
\begin{pmatrix}
1 & 0 & -1\\2 & 0 & -2\\1 & 0 & -1
\end{pmatrix}
```

Sobel filter

```math
\begin{pmatrix}
3 & 0 & -3\\10 & 0 & -10\\3 & 0 & -3
\end{pmatrix}
```

Scharr filter

In deep learning you don't need to hand-pick these nine numbers. Actually you can learn them as weights using backpropogation.

#### 1.4 Padding

n by n image convolve with f by f filter end up with **n-f+1** by n-f+1 result

##### Problems of convolution

1. every time shrink size
2. throwing away a lot of information of pixels near the edges/corners

Solution: pad the image, convention is pad with zeros.

p = padding, so output is **n + 2p - f + 1**

##### Valid and Same convolution

* "Valid": no padding
* "Same": pad so that output size is the same as the input size -> **p = (f-1)/2**, f is usually odd

#### 1.5 Strided convolutions 

s = stride

(n+2p-f)/s + 1

##### Technical note on cross-correlation vs. convolution

#### 1.6 Convolutions over volumes

e.g 6x6x3 * 3x3x3: height x width x channels

summary n x n x n_c  * f x f x n_c -> n - f + 1 x n - f + 1 x n_c' 

#### 1.7 One layer of a convolutional neural network

add bias and apply non-linearity 

###### Summary of notation

If layer l is a convolution layer

```math
f^{[l]}=filter size
```
```math
p^{[l]}=padding
```

