### How the Internet works 
This note is based on a tutorial on Khan academy called [Internet 101](https://www.khanacademy.org/computing/computer-science/internet-intro/).

#### wires, cables, and WiFi
Sending bits information
bandwidth: maximum transmission capacity of a device measured by 
bit rate: number of bits that we can send over a given period of time.
latency: time it takes for a bit to travel from one place to another

#### IP address and DNS
Internet is really a design philosophy expressed in a set of protocols 
Protocol is a well-known set of rules and standards used to communicate between machines
IP address: (IPv4) traditionally 4 parts, each 8 bits long. Country; Region; Subnetwork; Device
DNS servers are connected in a distributed hierarchy
DNS spoofing: matching domain name to a wrong address

#### Packet, routers and reliability
