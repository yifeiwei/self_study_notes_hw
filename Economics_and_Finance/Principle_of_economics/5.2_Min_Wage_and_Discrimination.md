## Key Concepts: Using the Labor Supply and Demand Model

• Differences in labor productivity are also an explanation for some of the differences in wages across groups, especially the differences between workers with different levels of education.

• There are various theories that attempt to explain wage differentials between union and non-union workers. One theory is that unions raise wages by restricting supply. By restricting membership, for example, they shift the labor supply curve to the left, raising wages, just as a monopolist raises the price of the good it sells by restricting supply. But when a union restricts supply, workers outside the union in another industry get paid less.

This effect of unions is illustrated in the figure below. The graph on the right is one industry; the graph on the left is another industry. Suppose both industries require workers of the same skill level. Imagine the situation before the union is formed. Then the wages for the workers on the left and on the right in the figure would be the same.

Now suppose a union organizes the industry on the left. Wages rise in the industry on the left, but the quantity of labor demanded in the industry falls. The workers in the industry on the left who become unemployed probably will move to the industry on the right. As they do so, the labor supply curve in the right-hand graph of the figure shifts and the wage in that industry declines. Thus, a wage gap between the similarly skilled union and nonunion workers is created.

![img](https://lagunita.stanford.edu/assets/courseware/v1/5a706e63f5a7a5f466407ad631d0edbd/asset-v1:HumanitiesSciences+Econ1V+Summer2018+type@asset+block/union_wage.png)

• However, there are other factors that bring about a disconnect between labor productivity and real wages. These include compensating wage differentials, discrimination, minimum wage laws, fixed wage contracts, and deferred wage payments.

• Compensating wage differentials occur because some jobs are more attractive than others. Workers with the same level of productivity will earn more in the sector that is less desirable to work in.

• Discrimination reduces the wages of those who are discriminated against below their marginal revenue product. Competition can be a force against the effects of discrimination because other firms can step in and hire the workers who are being discriminated against, offering to pay them a wage that is higher than what they are currently getting but still less than their marginal revenue product.

![img](https://lagunita.stanford.edu/assets/courseware/v1/2b9377c941c7affdaacfdbbb7524fda1/asset-v1:HumanitiesSciences+Econ1V+Summer2018+type@asset+block/discrim.png)

• Minimum wage laws can also lead to workers being paid a wage higher than their marginal revenue product. If the government imposes a minimum wage that is higher than the marginal revenue product of unskilled workers, it will cause some of these workers to lose their jobs, while the lucky ones earn a wage that is higher than what their marginal revenue product will indicate.

![img](https://lagunita.stanford.edu/assets/courseware/v1/82f26e0403e2a22479a32e6dad49d11d/asset-v1:HumanitiesSciences+Econ1V+Summer2018+type@asset+block/min_wage.png)

• Many labor market transactions are long term. Most employees receive a fixed hourly or weekly wage, even though their marginal revenue product fluctuates.

• In certain types of jobs, piece-rate contracts adjust the payment directly according to actual marginal product; they are a way to increase incentives to be more productive.

• Deferred compensation is another form of payment that aims at improving incentives and worker motivation.

## Key Terms: Labor Model Cont. – Min. Wage and Discrimination

**Compensating Wage Differential**: a difference in wages for people with similar skills based on some characteristic of the job, such as riskiness, discomfort, or inconvenience of the time schedule.

**Piece Rate System**: a system by which workers are paid a specific amount per unit they produce.

**Deferred Payment Contract**: an agreement between a worker and an employer whereby the worker is paid less than the marginal revenue product when young, and subsequently paid more than the marginal revenue product when old.

**Labor Union**: a coalition of workers, organized to improve the wages and working conditions of the members.

**Minimum Wage**: A wage set by the government below which firms may not pay (also known as a wage floor).