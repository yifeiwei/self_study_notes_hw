# Introduction to Visual Decoding
### Part 1 Definition and neural/behavioural observation
This part of notes is based on a 25 min long [video lecture](https://www.youtube.com/watch?v=VeFXGZyLBrw) given by [Li Zhaoping](http://www0.cs.ucl.ac.uk/staff/Zhaoping.Li) on the definitions and neural/behavioural observations of visual decoding. It comes with Chapter 6 of the book ["Understanding Vision: theory, model and data"](http://www0.cs.ucl.ac.uk/staff/Zhaoping.Li/VisionBook.html)

[![](http://img.youtube.com/vi/VeFXGZyLBrw/0.jpg)](http://www.youtube.com/watch?v=VeFXGZyLBrw "")


### Visual recognition as decoding
#### Definition
* Encoding 
* Selection
* Decoding 
    * infers from neural responses to scene property e.g face, color 
    * not every property of the visual scen is decoded
    * neural responses were selected by attention
    * r can be EEG, fMRI data

#### Visual decoding from monkeys and humans
"Where" - selection
"What" - decoding depend on time
#### Object invariance and sleoccurs after visual selection
* object invariance: recognize as the same object 
* visual selection by V1
* IT objection recognition
* X shape is no longer unique

#### Visual perception can be ambiguous
* nail illusion

#### Visual decoding involves topdown imagination: recognition by imagination or image synthesis
#### Neural basis of visual decoding
* V2/V4 involve
* V2 neural response -> perception which the outcome of neural decoding
* V2 neurons respond to illusory contours and can signal border ownership
* Many V4 neurons are also turned to border ownership
* Responses in V4 neurons are more likeky than V1/2 correlate with perception **decoded solution** other than retinal inputs **input representations**

### Part 2 Algorithms and examples
This part of notes is based on a 51 min long [video lecture](https://www.youtube.com/watch?v=bpzHeCi_y6E&feature=youtu.be) given by [Li Zhaoping](http://www0.cs.ucl.ac.uk/staff/Zhaoping.Li) on algorithms and examples of visual decoding. It comes with Chapter 6 of the book ["Understanding Vision: theory, model and data"](http://www0.cs.ucl.ac.uk/staff/Zhaoping.Li/VisionBook.html)


[![](http://img.youtube.com/vi/bpzHeCi_y6E/0.jpg)](http://www.youtube.com/watch?v=bpzHeCi_y6E "")


##### Maximum likelihood decoding
* Decision boundary

##### Visual recognition is often viewed in the framework of Bayesian inference
* Maximum postereriori decoding

```math
P(S|r)\propto P(r|S)P(S)
```

* Difference between perceived s and actual s is error

```math
\hat{s}-s
```
##### Seperation of two Gaussian Distribution - D prime

```math
d' = \frac{|\bar{R}(A)+\bar{R}(B)|}{\sqrt{0.5(\sigma^2(A)+\sigma^2(B)}}
```


```math
P(corret) = \frac{1}{\sqrt{\pi}} \int_{-\infty}^{d'/2} dx \exp (-x^2)
```
##### Example: discriminate two visual inputs (A and B) based on photoreceptor responses
$`\bar{r}(A) = (\bar{r_1}(A), \bar{r_2}(A)... \bar{r_i}(A)) `$ and $`\bar{r}(B) = (\bar{r_1}(B), \bar{r_2}(B)... \bar{r_i}(B)) `$

```math
P(r|\alpha) = \prod_i P(r_i|\alpha) = \prod_i (\frac{\bar{r_i}(\alpha)^{r_i}}{r_i !}\exp (-\bar{r_i}(\alpha)))
```

```math
P(r|A)>P(r|B) \rightarrow \frac{P(r|A)}{P(r|B)} \rightarrow \ln (\frac{P(r|A)}{P(r|B)})>0 
```
```math
\ln (\frac{P(r|A)}{P(r|B)}) =  \sum_i  \ln \frac{P(r_i|A)}{P(r_i |B)} r_i - \sum_i (\bar{r_i}(A)-\bar{r_i}(B)) > 0
```
* $`\sum_i (\bar{r_i}(A)-\bar{r_i}(B)) `$ is constant
* $`w_i = \ln \frac{P(r_i|A)}{P(r_i|B)} `$ is constant depending on A and B
* $`r_i`$: neural response in a particular trial which is a Poisson random variable
* $`R \equiv \sum w_i r_i`$
    * variance of R is another weighted sum of its mean

##### Just noticable difference
* discrimination threshold: the minimum difference between input A and input B to give a sufficiently large P(correct)
* sensitivity = 1/discrimination threshold
* Fisher information? (need to study)

#### Decoding accuracy and cue integration
##### Example: wavelength discrimination of monodromatic light
$`S = (\lambda, I) \rightarrow r=(r_L, r_M, r_S)`$
* Long, medium and short cones
* $` \bar{r_a} = I \cdot f_a(\lambda), a = L,M,S`$

37:00 need to work out fisher information




