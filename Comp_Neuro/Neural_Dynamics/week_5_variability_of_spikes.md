# Week 5: Variability of spikes

## Part 1 - Variability of spike trains

[![](http://img.youtube.com/vi/mmvNRkkJdJs/0.jpg)](http://www.youtube.com/watch?v=mmvNRkkJdJs "")  

Two questions

* Is timing of these action potentials fluctuating? Is it a variable?
* Where do these fluctuation of the membrane potentials come from?

Spike timing is not reliable. 

## Part 2 - Sources of Variability

[![](http://img.youtube.com/vi/4atNy9DJnPk/0.jpg)](http://www.youtube.com/watch?v=4atNy9DJnPk "")  

* Intrinsic noise (ion channels)
  * stochastic opening and closing
  * small contribution to variability
* Network noise (background noise)
  * spikes arrival from other neurons
  * checking intrinsic noise by removing the network (isolate a neuron)
  * big contribution to variability

## Part3a - Three definition of rate code

rate coding, 3 definitions

* Temporal averaging
  * rate as a (normalized) spike count $`v(t) = \frac{n^{sp}}{T}`$
    * Variability of interspike intervals (ISI) measures regularity of spike train
    * mean $` <n^{sp}>`$; 
    * Fano factor $`F = \frac{(n^{sp}_{k} - <n^{sp}_{k}>)^2}{<n^{sp}_{k}>}`$ measures repeatability across repetitions
  * slow code, not used by the neuron
* Averaging across repetitions
  * PSTH (Peri stimulus time histogram)
    * count number of spikes in a short time window across K repetitions divide by K and duration of counting interval 
    * $`PSTH(t) =\frac{n(t;t+\Delta t)}{K \Delta t}`$
    * it is a rate measure, a temporal resolution of firing rate 
    * single neuron/many trials; average across trials
  * Too slow, not possible for animals
* Population averaging ("spatial" averaging)
  * population activity - rate defined by population average
    * $` A(t) = \frac{n(t;t+\Delta t)}{N \Delta t} `$
    * natural

## Part 3b - Poisson model

Pure rate code = stochastic spiking -> poisson model

Homogeneous Poisson model: constant rate: constant firing numbers during time period T



